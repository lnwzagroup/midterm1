/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.midterm1;

/**
 *
 * @author Gigabyte
 */
public class TestSwitch {
    public static void main(String[] args) {
        Switch Red = new Switch("Red Switch","Linear",45);
        Switch Green = new Switch("Green Switch","Clicky",50);
        Switch Blue = new Switch("Blue Switch","Clicky",45);
        Switch Black = new Switch("Black Switch","Linear",60);
        Switch Brown = new Switch("Brown Switch","Tactile",50);
        System.out.println("Switch: "+Red.name+"\n"+"Type: "+Red.type+"\n"+"Pressure: "+Red.pressure);
        System.out.println("\n"+"Switch: "+Green.name+"\n"+"Type: "+Green.type+"\n"+"Pressure: "+Green.pressure);
        System.out.println("\n"+"Switch: "+Blue.name+"\n"+"Type: "+Blue.type+"\n"+"Pressure: "+Blue.pressure);
        System.out.println("\n"+"Switch: "+Black.name+"\n"+"Type: "+Black.type+"\n"+"Pressure: "+Black.pressure);
        System.out.println("\n"+"Switch: "+Brown.name+"\n"+"Type: "+Brown.type+"\n"+"Pressure: "+Brown.pressure);
                
    }
    
}
